﻿using Microsoft.EntityFrameworkCore;
using Shared.Interfaces;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlAccessLibrary
{
    public class EFCategoryRepository : ICategoryRepository
    {
        #region Constructor region

        private readonly GeneratorDatabaseContext _context;

        public EFCategoryRepository(GeneratorDatabaseContext context)
        {
            _context = context;
        }

        #endregion Constructor region


        public CategoryModel this[int id] => _context.Categories.Find(id);

        public IEnumerable<CategoryModel> Categories => _context.Categories;

        public void CreateCategory(string newCategoryName)
        {
            CategoryModel newCategory = new CategoryModel() { CategoryName = newCategoryName };
            _context.Categories.Add(newCategory);
            _context.SaveChanges();
        }

        public bool DeleteCategory(int id, IEnumerable<ProblemModel> problems = null)
        {
            bool operationStatus;

            CategoryModel categoryToDelete = _context.Categories.Find(id);

            if (categoryToDelete == null)
            {
                operationStatus = false;
            }
            else if (_context.Questions.Select(prob => prob.Category).Where(selected => selected.CategoryId == id).Any())
            {
                operationStatus = false;
            }
            else
            {
                _context.Categories.Remove(categoryToDelete);
                _context.SaveChanges();
                operationStatus = true;
            }

            return operationStatus;
        }

        public CategoryModel ReadCategory(int id) => _context.Categories.Find(id);

        public bool UpdateCategory(int id, string updatedCategoryName)
        {
            CategoryModel categoryToUpdate = _context.Categories.Find(id);

            if (categoryToUpdate == null)
            {
                return false;
            }

            categoryToUpdate.CategoryName = updatedCategoryName;
            _context.SaveChanges();
            return true;
        }
    }
}
