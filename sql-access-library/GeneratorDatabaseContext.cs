﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlAccessLibrary
{
    public class GeneratorDatabaseContext : DbContext
    {
        public GeneratorDatabaseContext(DbContextOptions options) : base(options)
        {
            // Empty
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Converter & Comparer are required to (de-)serialize a list of numbers 
            var converter = new ValueConverter<List<int>, string>
            (
                v => string.Join(";", v),
                v => v.Split(";", StringSplitOptions.RemoveEmptyEntries).Select(val => int.Parse(val)).ToList()
            );

            modelBuilder.Entity<ExamModel>()
                .Property(e => e.ChosenProblemIds)
                .HasConversion(converter);

            var comparer = new ValueComparer<List<int>>(
                    (c1, c2) => c1.SequenceEqual(c2),
                    c => c.Aggregate(0, (a, v) => HashCode.Combine(a, v.GetHashCode())),
                    c => c.ToList());

            modelBuilder.Entity<ExamModel>()
                .Property(e => e.ChosenProblemIds)
                .Metadata
                .SetValueComparer(comparer);
        }

        public DbSet<CategoryModel> Categories { get; set; }
        public DbSet<ProblemModel> Questions { get; set; }
        public DbSet<ExamModel> Exams { get; set; }
    }
}
