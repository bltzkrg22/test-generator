﻿using Microsoft.EntityFrameworkCore;
using Shared.Interfaces;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlAccessLibrary
{
    public class EFQuestionRepository : IQuestionRepository
    {
        #region Constructor region

        private readonly GeneratorDatabaseContext _context;

        public EFQuestionRepository(GeneratorDatabaseContext context)
        {
            _context = context;
        }

        #endregion Constructor region



        public ProblemModel this[int id] => _context.Questions.Find(id);

        public IEnumerable<ProblemModel> Problems => _context.Questions;

        public ProblemModel CreateProblem(ProblemModel problem)
        {
            _context.Questions.Add(problem);
            _context.SaveChanges();
            return problem;
        }

        public bool DeleteProblem(int id)
        {
            bool operationStatus;
            ProblemModel problemToDelete = _context.Questions.Find(id);

            if (problemToDelete == null)
            {
                operationStatus = false;
            }

            _context.Remove(problemToDelete);
            operationStatus = true;

            _context.SaveChanges();
            return operationStatus;
        }

        public IList<int> FilterIdsOfProblems(ProblemType type, ISet<int> selectedCategoryIds)
        {
            if (selectedCategoryIds is not null && selectedCategoryIds.Count > 0)
            {
                return _context.Questions.Where(prob => prob.Type == type && selectedCategoryIds.Contains(prob.IdNumber))
                                         .Select(prob => prob.IdNumber).ToList();
            }
            else
            {
                return _context.Questions.Where(prob => prob.Type == type).Select(prob => prob.IdNumber).ToList();
            }
        }

        public IList<ProblemModel> ReadMultipleProblems(IList<int> ids)
        {
            // The ceremony is required so that the returned items are in the same order as ids from input
            IQueryable<ProblemModel> unorderedProblems = _context.Questions.Where(prob => ids.Contains(prob.IdNumber));

            List<ProblemModel> orderedProblems = 
                ids.Join(unorderedProblems, id => id, uordProb => uordProb.IdNumber, (id, uordProb) => uordProb).ToList();
            return orderedProblems;
        }

        public ProblemModel ReadProblem(int id) => _context.Questions.Find(id);


        public ProblemModel UpdateProblem(int id, ProblemModel problem)
        {
            var problemToUpdate = _context.Questions.Find(id);

            if (problemToUpdate == null)
            {
                return null;
            }

            problemToUpdate = problem;
            _context.SaveChanges();
            return problem;
        }

    }
}
