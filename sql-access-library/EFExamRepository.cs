﻿using Microsoft.EntityFrameworkCore;
using Shared.Interfaces;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlAccessLibrary
{
    public class EFExamRepository : IExamRepository
    {
        #region Constructor region

        private readonly GeneratorDatabaseContext _context;

        public EFExamRepository(GeneratorDatabaseContext context)
        {
            _context = context;
        }

        #endregion Constructor region



        public IEnumerable<ExamModel> GeneratedExams => _context.Exams;

        public bool DeleteExam(int id)
        {
            bool operationStatus;
            ExamModel examToDelete = _context.Exams.Find(id);

            if (examToDelete == null)
            {
                operationStatus = false;
            }

            _context.Remove(examToDelete);
            operationStatus = true;

            _context.SaveChanges();
            return operationStatus;
        }

        public bool OverwriteExam(ExamModel examToOverwriteWith, int id)
        {
            var examToUpdate = _context.Exams.Find(id);

            if (examToUpdate == null)
            {
                return false;
            }

            examToUpdate = examToOverwriteWith;
            _context.SaveChanges();
            return true;
        }

        public ExamModel RetrieveExam(int id) => _context.Exams.Find(id);


        public int StoreExam(ExamModel examToStore)
        {
            _context.Exams.Add(examToStore);
            _context.SaveChanges();
            return examToStore.DbId;
        }
    }
}
