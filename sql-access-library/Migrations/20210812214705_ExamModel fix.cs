﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace sql_access_library.Migrations
{
    public partial class ExamModelfix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Questions_Exams_ExamModelDbId",
                table: "Questions");

            migrationBuilder.DropIndex(
                name: "IX_Questions_ExamModelDbId",
                table: "Questions");

            migrationBuilder.DropColumn(
                name: "ExamModelDbId",
                table: "Questions");

            migrationBuilder.AddColumn<string>(
                name: "ChosenProblemIds",
                table: "Exams",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChosenProblemIds",
                table: "Exams");

            migrationBuilder.AddColumn<int>(
                name: "ExamModelDbId",
                table: "Questions",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Questions_ExamModelDbId",
                table: "Questions",
                column: "ExamModelDbId");

            migrationBuilder.AddForeignKey(
                name: "FK_Questions_Exams_ExamModelDbId",
                table: "Questions",
                column: "ExamModelDbId",
                principalTable: "Exams",
                principalColumn: "DbId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
