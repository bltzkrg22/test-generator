﻿using Shared.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Generator.ViewModels
{
    public class GenerationViewModel
    {
        [Required]
        [Display(Name = "Liczba zadań zamkniętych")]
        public int ClosedCount { get; set; }

        [Required]
        [Display(Name = "Liczba zadań otwartych")]
        public int OpenCount { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Tytuł")]
        public string Title { get; set; }

        // I *need* an indexer in views, hence IList and not IEnumerable here 
        public IList<CategorySelection> CategoriesSelected { get; set; }
    }


    // Helper class to pass the value of checkbox into/from view
    public class CategorySelection
    {
        public CategoryModel Category { get; set; }
        public bool IsSelected { get; set; } = false;
    }
}
