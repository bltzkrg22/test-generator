﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Interfaces;
using Shared.Models;

namespace Generator.ViewModels
{
    public class ExamSummaryViewModel
    {
        public ExamModel Exam {get; set;}

        public List<ProblemModel> ChosenProblemList { get; set; }

        public int ExamDbId { get; set; }

        // Values below deliberately initialized to invalid values
        public int IdOfProblemToInsert { get; set; }

        public int PositionOfProblemToInsert { get; set; }

        public int PositionOfProblemToRemove { get; set; }

        public int NumberOfStudents { get; set; } = 1;
    }
}
