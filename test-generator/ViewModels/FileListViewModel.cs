﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Generator.ViewModels
{
    public class FileModel
    {
        public string FileName { get; set; }
        public bool FreshnessStatus { get; set; }
    }

    public class FileListViewModel
    {
        public IEnumerable<FileModel> FileList { get; set; }
    }
}
