﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Generator.Helpers
{
    public static class StringHelpers
    {
        public static string TruncateLongString(this string str, int maxLength)
        {
            if (String.IsNullOrEmpty(str)) return str;

            if (str.Length <= maxLength)
            {
                return str;
            }
            else
            {
                return str.Substring(0, maxLength) + " […]";
            }
        }

        public static string SantizeFileName(string name)
        {
            string truncatedName = name.TruncateLongString(GlobalConstants.TitleToFilenameCharacterCount);

            // Characters I don’t want in generated filenames 
            char[] localInvalidCharsArray = new char[] { ' ', '.', ',' };
            char[] systemInvalidCharsArray = System.IO.Path.GetInvalidFileNameChars();

            string invalidChars = Regex.Escape(new String(systemInvalidCharsArray)) + Regex.Escape(new String(localInvalidCharsArray));
            string invalidRegStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);

            return Regex.Replace(truncatedName, invalidRegStr, "_");
        }
    }
}
