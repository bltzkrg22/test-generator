﻿using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace Generator.Helpers
{
    public static class AttributeGetter
    {
        public static string DisplayName<T>(string propertyName)
        {
            MemberInfo property = typeof(T).GetProperty(propertyName);
            return property.GetCustomAttribute<DisplayAttribute>()?.Name;
        }
    }
}