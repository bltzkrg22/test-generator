﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Generator
{
    public static class GlobalConstants
    {
        public const int PaginationSize = 10;
        
        // How many characters from exam title are copied into the filename of 
        // generated .tex file
        public const int TitleToFilenameCharacterCount = 22;
    }
}
