﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Generator.BlazorComponents
{
    public partial class Counter
    {
        private int currentCount = 0;

        [Parameter]
        public int InitialValue { get; set; }

        private void IncrementCount() => currentCount++;

        protected override void OnParametersSet()
        {
            currentCount = InitialValue;
        }
    }
}
