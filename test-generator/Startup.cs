using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

using Shared.Interfaces;
using Shared.Models;
using Shared.MockupData;
using Generator.Helpers;

using SqlAccessLibrary;


namespace Generator
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<GeneratorDatabaseContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("Default"))
            );


            //services.AddSingleton<ICategoryRepository, MemoryCategoryRepository>();
            //services.AddSingleton<IQuestionRepository, MemoryQuestionRepository>();
            //services.AddSingleton<IExamRepository, MemoryExamRepository>();

            services.AddTransient<ICategoryRepository, EFCategoryRepository>();
            services.AddTransient<IQuestionRepository, EFQuestionRepository>();
            services.AddTransient<IExamRepository, EFExamRepository>();

            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();

                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapBlazorHub();
            });
        }
    }
}
