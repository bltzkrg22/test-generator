﻿using Generator.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Shared.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Generator.Controllers
{
    public class FilesController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly string _rootDirectory;

        public FilesController(IConfiguration config)
        {
            _configuration = config;
            _rootDirectory = _configuration.GetValue<string>(WebHostDefaults.ContentRootKey);
        }

        [Route("Download/{fileName}")]
        public IActionResult Download(string fileName)
        {
            if (fileName == null)
            {
                return NotFound();
            }

            string path = Path.Join(_rootDirectory, @".\ExamFilesOutput\", fileName);
            byte[] bytes = System.IO.File.ReadAllBytes(path);
            return PhysicalFile(path, "application/octet-stream", fileName);
        }

        [Route("Files/")]
        public IActionResult Index()
        {
            string outputDirectory = Path.Join(_rootDirectory, @".\ExamFilesOutput\");
            var filePaths = Directory.EnumerateFiles(outputDirectory).Where(path => path.ToLower().EndsWith(".tex"));

            IEnumerable<FileModel> viewModel = filePaths.Select
            (
                path => new FileModel()
                { 
                    FileName = Path.GetFileName(path), 
                    FreshnessStatus = TimeHelpers.IsFileFresh(System.IO.File.GetCreationTime(path))
                }
            );

            return View(viewModel);
        }
    }
}
