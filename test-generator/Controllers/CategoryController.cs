﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Shared.Models;
using Shared.MockupData;
using Shared.Interfaces;




namespace Generator.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryRepository _categories;
        private readonly IQuestionRepository _questions;

        public CategoryController(ICategoryRepository categories, IQuestionRepository questions)
        {
            _categories = categories;
            _questions = questions;
        }

        public IActionResult Index()
        {
            IEnumerable<CategoryModel> categories = _categories.Categories;
            return View(categories);
        }

        public IActionResult Read(int? id)
        {
            CategoryModel category = null;
            if (id is not null)
            {
                category = _categories.ReadCategory(id.Value);
            }

            if (category is not null)
            {
                return View(category);
            }
            else
            {
                return NotFound();
            }
        }

        public IActionResult Update(int id)
        {
            var category = _categories.ReadCategory(id);

            return View(category);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(CategoryModel category)
        {
            if (!ModelState.IsValid)
            {
                return View(category);
            }

            int updateId = category.CategoryId;

            _categories.UpdateCategory(updateId, category.CategoryName);

            return RedirectToAction("Index");
        }


        public IActionResult Create()
        {
            var category = new CategoryModel();

            return View(category);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CategoryModel category)
        {
            if (!ModelState.IsValid)
            {
                return View(category);
            }

            _categories.CreateCategory(category.CategoryName);

            return RedirectToAction("Index");
        }



        public IActionResult Delete(int id)
        {
            var categoryToDelete = _categories.ReadCategory(id);

            return View(categoryToDelete);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(CategoryModel category)
        {
            // EF ignores parameter problems:; it is only used, when in-memory repository is used
            bool canBeDeleted = _categories.DeleteCategory(category.CategoryId, problems: _questions.Problems);

            if (canBeDeleted == false)
            {
                ModelState.AddModelError("CustomError", "Nie można usunąć kategorii, dopóki istnieją zadania do niej przypisane!");
            }

            if (!ModelState.IsValid)
            {
                return View(category);
            }

            return RedirectToAction("Index");
        }

        private IActionResult Read(ProblemModel problem)
        {
            return View(problem);
        }
    }
}
