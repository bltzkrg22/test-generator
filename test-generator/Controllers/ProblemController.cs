﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Generator.Helpers;

using Shared.Models;
using Shared.MockupData;
using Shared.Interfaces;

using Microsoft.AspNetCore.Mvc.Rendering;

using X.PagedList;

namespace Generator.Controllers
{
    public class ProblemController : Controller
    {
        private IQuestionRepository _questions;
        private readonly ICategoryRepository _categories;

        public ProblemController(IQuestionRepository questions, ICategoryRepository categories)
        {
            _questions = questions;
            _categories = categories;
        }

        public IActionResult Index(int? category, int? page)
        {
            IEnumerable<ProblemModel> problems;
            if (category is null || category == 0)
            {
                problems = _questions.Problems;
            }
            else // if (category is not null)
            {
                problems = _questions.Problems.Where(problem => problem.Category.CategoryId == category.Value);
            }

            var CategoriesWithDefault = new List<CategoryModel>()
                { new CategoryModel() { CategoryId = 0, CategoryName = "Wszystkie kategorie" } };
            CategoriesWithDefault.AddRange(_categories.Categories);

            ViewBag.CategoriesWithDefault = CategoriesWithDefault;
            //ViewBag.CategoryDropdownList = new SelectList(CategoriesWithDefault, "CategoryId", "CategoryName",
            //    selectedValue: CategoriesWithDefault.FirstOrDefault(cat => cat.CategoryId == category.Value));

            // Id of current category
            ViewBag.preservedCategory = category;
            ViewBag.preservedPage = page;

            if (category is not null)
            {
                ViewBag.selectedCategoryName = _categories.ReadCategory(category.Value)?.CategoryName ?? String.Empty;
            }

            int pageNumber = page ?? 1;
            return View(problems.ToPagedList(pageNumber, GlobalConstants.PaginationSize));
        }

        public IActionResult Read(int? id, int? category, int? page)
        {
            ViewBag.CategoryList = new SelectList(_categories.Categories, "CategoryId", "CategoryName");

            ViewBag.preservedCategory = category;
            ViewBag.preservedPage = page;

            ProblemModel problem = null;
            if (id is not null)
            {
                problem = _questions.ReadProblem(id.Value);
            }

            if (problem is not null)
            {
                return View(problem);
            }
            else
            {
                return NotFound();
            }
        }

        public IActionResult Update(int id, int? category, int? page)
        {
            var catList = _categories.Categories;
            ViewBag.CategoryList = new SelectList(catList, "CategoryId", "CategoryName");

            ViewBag.preservedCategory = category;
            ViewBag.preservedPage = page;

            var problemToUpdate = _questions.ReadProblem(id);

            return View(problemToUpdate);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(ProblemModel problem, int? category, int? page)
        {
            var catList = _categories.Categories;
            ViewBag.CategoryList = new SelectList(catList, "CategoryId", "CategoryName");

            if (_categories.ReadCategory(problem.Category.CategoryId) is null)
            {
                ModelState.AddModelError("CustomError", "Kategoria nie jest zdefiniowana w bazie.");
            }

            
            int updateId = problem.IdNumber;

            if (ModelState.IsValid)
            {
                // The form posts only CategoryId, so we read full category object from database
                problem.Category = _categories.ReadCategory(problem.Category.CategoryId);

                _questions.UpdateProblem(updateId, problem);
                return RedirectToAction("Read", new { id = updateId, category = category, page = page });
            }
            else
            {
                // error while updating
                return View(problem);
            }
        }

        public IActionResult Create(int? category, int? page)
        {
            var catList = _categories.Categories;
            ViewBag.CategoryList = new SelectList(catList, "CategoryId", "CategoryName");
            // category is a routing parameter!

            ViewBag.preservedCategory = category;
            ViewBag.preservedPage = page;

            var newProblem = new ProblemModel();

            CategoryModel categoryFromRouting = null;
            if (category is not null && category.Value != 0)
            {
                categoryFromRouting = _categories.ReadCategory(category.Value);
            }
            if (categoryFromRouting is not null)
            {
                newProblem.Category = categoryFromRouting;
            }

            return View(newProblem);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ProblemModel problem, int? category, int? page)
        {
            var catList = _categories.Categories;
            ViewBag.CategoryList = new SelectList(catList, "CategoryId", "CategoryName");

            if (_categories.ReadCategory(problem.Category.CategoryId) is null)
            {
                ModelState.AddModelError("CustomError", "Kategoria nie jest zdefiniowana w bazie.");
            }

            if (ModelState.IsValid)
            {
                problem.Category = _categories.ReadCategory(problem.Category.CategoryId);

                _ = _questions.CreateProblem(problem);

                return RedirectToAction("Read", new { id = problem.IdNumber, category = category, page = page });
            }
            else
            {
                // error while creating
                return View(problem);
            }
        }

        public IActionResult Delete(int id, int? category, int? page)
        {
            var catList = _categories.Categories;
            ViewBag.CategoryList = new SelectList(catList, "CategoryId", "CategoryName");

            ViewBag.preservedCategory = category;
            ViewBag.preservedPage = page;

            var problemToDelete = _questions.ReadProblem(id);

            return View(problemToDelete);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(ProblemModel problem, int? category, int? page)
        {
            _questions.DeleteProblem(problem.IdNumber);

            return RedirectToAction("Index", new { category = category, page = page });
        }

        //private IActionResult Read(ProblemModel problem)
        //{
        //    return View(problem);
        //}

        //private int? PreserveCategory()
        //{
            // Preserve routing category; 
            //if (this.RouteData.Values["category"] is null)
            //{
            //    return null;
            //}

            // .ToString, in case user manually inputs some garbage
            //bool isParseSuccessful = Int32.TryParse(this.HttpContext.Request.Query["category"].ToString(), out int currentCategory);
            //if (isParseSuccessful)
            //{
            //    return currentCategory;
            //}
            //else
            //{
            //    return null;
            //}
        //}
    }
}
