﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Generator.Helpers;

using Shared.Models;
using Shared.MockupData;
using Shared.Interfaces;

using Microsoft.AspNetCore.Mvc.Rendering;
using Generator.ViewModels;
using Shared;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Shared.Helpers;

namespace Generator.Controllers
{
    public class GenerateController : Controller
    {
        private readonly ICategoryRepository _categories;
        private readonly IQuestionRepository _questions;
        private readonly IConfiguration _configuration;

        private IExamRepository _exams;

        public GenerateController(ICategoryRepository categories, IQuestionRepository questions, IExamRepository exams, IConfiguration configuration)
        {
            _categories = categories;
            _questions = questions;
            _exams = exams;
            _configuration = configuration;
        }


        public IActionResult Index()
        {
            IEnumerable<ExamModel> exams = _exams.GeneratedExams;
            return View(exams);
        }



        public IActionResult RemoveExam(int? examId)
        {
            if (examId is null)
            {
                return RedirectToAction("Index");
            }

            bool deletionStatus = _exams.DeleteExam(examId.Value);

            return RedirectToAction("Index");
        }





        public IActionResult SetConstraints()
        {
            var viewmodel = new GenerationViewModel()
            {
                // IEnumerable<(CategoryModel cat, bool IsSelected)>, all checboxes initialized to unchecked = false
                CategoriesSelected = _categories.Categories.Select( cat => new CategorySelection() { Category = cat, IsSelected = false } ).ToList(),
            };

            return View(viewmodel);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SetConstraints(GenerationViewModel viewmodel)
        {
            if (ModelState.IsValid)
            {
                // TempData["GenerationViewModel"] = viewmodel;
                TempData.Put("GenerationViewModel", viewmodel);
                return RedirectToAction("Confirm");
            }
            else
            {
                return View(viewmodel);
            }


            //HashSet<int> selectedCategoriesIds = null;

            //if (ModelState.IsValid)
            //{
            //    if (viewmodel.Constraints.CategoriesSelected.Count > 0)
            //    {
            //        // ToHashSet to automagically remove duplicates; not sure how selection will be implemented yet
            //        selectedCategoriesIds = viewmodel.Constraints.CategoriesSelected.Select(cat => cat.CategoryId).ToHashSet();
            //    }
            //}
            //else
            //{
            //    // error
            //    return View(viewmodel);
            //}
        }


        public IActionResult Confirm()
        {
            // GenerationViewModel viewmodel = TempData["GenerationViewModel"] as GenerationViewModel;
            GenerationViewModel viewmodel = TempData.Get<GenerationViewModel>("GenerationViewModel");

            if (viewmodel is null)
            {
                return RedirectToAction("SetConstraints");
            }

            return View(viewmodel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Confirm(GenerationViewModel viewmodel)
        {
            IEnumerable<int> catIdsSelected = viewmodel.CategoriesSelected.Where(c => c.IsSelected).Select(c => c.Category.CategoryId);

            GenerationConstraintsModel constraintsFromView = new GenerationConstraintsModel()
            {
                GUID = Guid.NewGuid(),
                ClosedCount = viewmodel.ClosedCount,
                OpenCount = viewmodel.OpenCount,
                CategoryIdsSelected = catIdsSelected.ToHashSet(),
                Title = viewmodel.Title
            };

            ExamMethods examInProgress = ProblemSelector.SelectProblemsForExam(_questions, constraintsFromView);
            int generatedExamId = _exams.StoreExam(examInProgress.SaveData());
            TempData["generatedExamId"] = generatedExamId;

            return RedirectToAction("Summary");
            //_ = Task.Run(() => _emailService.SendEmailAsync(viewmodel.Constraints));


        }

        public IActionResult Summary(int? examId)
        {
            // GenerationViewModel viewmodel = TempData["GenerationViewModel"] as GenerationViewModel;

            int? generatedExamId = TempData["generatedExamId"] as int?;
            if (generatedExamId is null && examId is null)
            {
                return RedirectToAction("SetConstraints");
            }
            // examId from routing is preferred over TempData
            else if (examId is not null)
            {
                generatedExamId = examId;
            }

            ExamModel examInProgress = _exams.RetrieveExam(generatedExamId.Value);
            if (examInProgress is null)
            {
                return RedirectToAction("SetConstraints");
            }

            ExamSummaryViewModel viewmodel = new ExamSummaryViewModel
            {
                ChosenProblemList = _questions.ReadMultipleProblems(examInProgress.ChosenProblemIds).ToList(),
                Exam = examInProgress,
                ExamDbId = generatedExamId.Value
            };

            return View(viewmodel);
        }


        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public IActionResult Summary(ExamSummaryViewModel viewmodel)
        //{

        //    return View(viewmodel);
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Remove(ExamSummaryViewModel viewmodel)
        {
            ExamMethods examMethods = new ExamMethods(_questions);
            examMethods.LoadData(_exams.RetrieveExam(viewmodel.ExamDbId));

            if (0 <= viewmodel.PositionOfProblemToRemove && viewmodel.PositionOfProblemToRemove < examMethods.ChosenProblems.Count)
            {
                examMethods.RemoveProblem(viewmodel.PositionOfProblemToRemove);
            }

            _exams.OverwriteExam(examMethods.SaveData(), viewmodel.ExamDbId);

            return RedirectToAction("Summary", new { examId = viewmodel.ExamDbId });
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Insert(ExamSummaryViewModel viewmodel)
        {
            ExamMethods examMethods = new ExamMethods(_questions);
            examMethods.LoadData(_exams.RetrieveExam(viewmodel.ExamDbId));

            ProblemModel problemToInsert = _questions.ReadProblem(viewmodel.IdOfProblemToInsert);
            if (problemToInsert is not null)
            {
                if (0 <= viewmodel.PositionOfProblemToInsert && viewmodel.PositionOfProblemToInsert < examMethods.ChosenProblems.Count)
                {
                    examMethods.AddProblem(problemToInsert, viewmodel.PositionOfProblemToInsert);
                }
                else
                {
                    examMethods.AddProblem(problemToInsert);
                }
            }

            _exams.OverwriteExam(examMethods.SaveData(), viewmodel.ExamDbId);

            return RedirectToAction("Summary", new { examId = viewmodel.ExamDbId });
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Print(ExamSummaryViewModel viewmodel)
        {

            return RedirectToAction("Print", new { examId = viewmodel.ExamDbId, numberOfStudents = viewmodel.NumberOfStudents });
        }



        public IActionResult Print(int? examId, int? numberOfStudents)
        {
            // GenerationViewModel viewmodel = TempData["GenerationViewModel"] as GenerationViewModel;

            if (numberOfStudents is null || numberOfStudents.Value <= 0)
            {
                numberOfStudents = 1;
            }


            if (examId is null)
            {
                return NotFound();
            }

            ExamModel examToPrint = _exams.RetrieveExam(examId.Value);

            if (examToPrint is null)
            {
                return NotFound();
            }

            ExamPrinter examPrinter = new ExamPrinter(_configuration, _questions);
            examPrinter.LoadExamModel(examToPrint);
            examPrinter.NumberOfStudents = numberOfStudents.Value;

            string rootDirectory = _configuration.GetValue<string>(WebHostDefaults.ContentRootKey);
            string pathToTemplate = Path.Join(rootDirectory, @".\ExamFilesTemplates\Template.tex");
            StreamReader templateFile = new StreamReader(pathToTemplate);

            string timestamp = TimeHelpers.GetTimestamp();
            string sanitizedTitle = StringHelpers.SantizeFileName(examToPrint.Title);
            string pathToOutput = Path.Join(rootDirectory, @$".\ExamFilesOutput\Outputfile-{timestamp}-{sanitizedTitle}.tex");

            // namespace must be explicit, method with name File is also in ContollerBase class!
            System.IO.File.WriteAllBytes(pathToOutput, examPrinter.PrintLatexFile(templateFile).ToArray());

            return Redirect("/Generate/Complete");
        }
    }
}
