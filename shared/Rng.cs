﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Shared
{
    public static class Rng
    {
        private static Random rng = new Random();

        public static int GetRandom() => rng.Next();

        /// <summary>
        /// Generates a random number in range from 0 to (<paramref name="max"/> - 1), inclusive.
        /// </summary>
        public static int GetRandom(int max) => rng.Next(max);

        /// <summary>
        /// Generates a random number in range from <paramref name="min"/> to (<paramref name="max"/> - 1), inclusive.
        /// </summary>
        public static int GetRandom(int min, int max) => rng.Next(min, max);




        /// <summary>
        /// Generates a random number in range from <paramref name="min"/> to <paramref name="max"/> (!), inclusive. The difference between
        /// the random number and min must be a multiple of <paramref name="step"/>.
        /// </summary>
        public static dynamic GetRandomStep(dynamic min, dynamic max, dynamic step)
        {
            // very dirty trick, but since I provide data on my own
            // I have some limited confidence it *should* work

            int n = (int)((max - min) / step);
            int r = Rng.rng.Next(0, n + 1);
            return min + step * r;
        }


        /// <summary>
        /// Shuffles only the first <paramref name="shuffleCount"/> values stored in a list/array using Knuth shuffle
        /// (i.e. Fisher-Yates shuffle) algorithm.
        /// </summary>
        public static void KnuthShuffle<T>(IList<T> list, int shuffleCount)
        {
            for (int i = 0; i < shuffleCount; i++)
            {
                int j = Rng.GetRandom(i, list.Count);
                T swapBuffer = list[i];
                list[i] = list[j];
                list[j] = swapBuffer;
            }
        }

        /// <summary>
        /// Shuffles all values stored in a list/array using Knuth shuffle (i.e. Fisher-Yates shuffle) algorithm.
        /// </summary>
        public static void KnuthShuffle<T>(IList<T> list)
        {
            KnuthShuffle(list, list.Count);
        }


        /// <summary>
        /// Shuffles only the values stored in a dictionary. After shuffling each key will be mapped
        /// to a random, previously existing value.
        /// </summary>
        public static void DictShuffle<T, U>(Dictionary<T, U> dict)
        {
            int n = dict.Count;

            T[] keyArray = dict.Keys.ToArray();
            //KnuthShuffle(keyArray);

            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                U value = dict[keyArray[k]];
                dict[keyArray[k]] = dict[keyArray[n]];
                dict[keyArray[n]] = value;
            }
        }

    }
}
