﻿using NReco.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Shared
{
    public static class ProblemMutator
    {
        // Prepopulating varContext dictionary is a workaround that allows using
        // some math functions by NReco LambdaParser 
        private readonly static Dictionary<string, object> varContextInitials = new Dictionary<string, object>() {
                { "Sqrt", (Func<decimal, decimal>)(a => (decimal)Math.Sqrt(decimal.ToDouble(a))) },
                { "Floor", (Func<decimal, decimal>)(a => Math.Floor(a)) },
                { "Ceil", (Func<decimal, decimal>)(a => Math.Ceiling(a)) },
                { "Pow", (Func<decimal, decimal, decimal>)((a,b) => (decimal)Math.Pow(decimal.ToDouble(a),decimal.ToDouble(b))) },
            };

        public static string ProblemTextMutator(string problemOriginalText)
        {
            // CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
            int maxRerolls = LibraryConstants.MaxMutatorRerolls;

            string[] fileContentArray = problemOriginalText.Split("%% VARIABLES", StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
            StringBuilder problemText = new StringBuilder(fileContentArray[0], fileContentArray[0].Length + 1024); // MAGIC NUMBER!

            string problemVariablesAndExpressions;
            try
            {
                problemVariablesAndExpressions = fileContentArray[1];
            }
            catch (IndexOutOfRangeException)
            {
                // There was no "%% VARIABLES" section in the problem description. There's nothing to mutate!
                return problemOriginalText;
                //throw;
            }


            var lambdaParser = new LambdaParser();
            var varContext = new Dictionary<string, object>(varContextInitials);
            var replaceStrings = new Dictionary<string, string>();
            var answerTemporaryReplaceStrings = new Dictionary<string, string>();
            var answerLabelsShuffleMap = new Dictionary<string, string>();


            // It is possible that for a set of randomly chosen vars answers repeat
            // In that case, try to repeat the generation up to 3 times 
            bool noAnswersRepeat = false;
            int countRestarts = 0;

            while (!noAnswersRepeat && countRestarts <= maxRerolls)
            {
                // Reset the data collections populated with variables read from inputfile
                // in case there are duplicate answer and we have to reroll
                varContext = new Dictionary<string, object>(varContextInitials);
                replaceStrings = new Dictionary<string, string>();
                answerTemporaryReplaceStrings = new Dictionary<string, string>();
                answerLabelsShuffleMap = new Dictionary<string, string>();

                foreach (var line in problemVariablesAndExpressions.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries))
                {
                    Regex variableRegex = new Regex(@"^% (var\d+);(\w):(.*?)$");
                    Regex expressionRegex = new Regex(@"^% (exp\d+|ans\d+|tof\d+);(.*?$)");
                    Regex trueorfalseRegex = new Regex(@"^% (tof\d+|ans\d+);(.*?$)");
                    Regex answerRegex = new Regex(@"^% (ans\d+);(.*?$)");

                    if (variableRegex.IsMatch(line))
                    {
                        var matches = variableRegex.Matches(line);

                        // matches[0].Groups[1].ToString() = "var1" or "var10" or whatever
                        string varName = matches[0].Groups[1].ToString();
                        // matches[0].Groups[2].ToString() = type of variable (single value, range, list, …)
                        string varDefinition = matches[0].Groups[2].ToString();
                        // matches[0].Groups[3].ToString() = parameters for a particular type
                        string[] varParameters = matches[0].Groups[3].ToString().Split(';', StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

                        dynamic varResult = null;


                        switch (varDefinition[0])
                        {
                            case 'v':
                                varContext[varName] = DynamicParseToNumber(varParameters[0]);
                                varResult = varContext[varName].ToString();
                                break;
                            case 'r':
                                dynamic minRange = DynamicParseToNumber(varParameters[0]);
                                dynamic maxRange = DynamicParseToNumber(varParameters[1]);
                                dynamic step = DynamicParseToNumber(varParameters[2]);

                                varContext[varName] = Rng.GetRandomStep(minRange, maxRange, step);
                                varResult = varContext[varName].ToString();
                                break;
                            case 'l':
                                int randomIndex = Rng.GetRandom(varParameters.Length);
                                int tryInt;
                                decimal tryDecimal;

                                if (int.TryParse(varParameters[randomIndex], out tryInt))
                                {
                                    varResult = tryInt;
                                }
                                else if (decimal.TryParse(varParameters[randomIndex], out tryDecimal))
                                {
                                    varResult = tryDecimal;
                                }
                                else
                                {
                                    // if string variable is defined with quotes, remove them
                                    var stripQuotes = varParameters[randomIndex].Replace("\"", "");
                                    varResult = stripQuotes;
                                }

                                varContext[varName] = varResult;
                                break;
                            default:
                                // NEW EXCEPTION GOES HERE!!!
                                break;
                        }


                        replaceStrings.Add(varName, varResult.ToString());
                    }
                    else if (expressionRegex.IsMatch(line))
                    {
                        var matches = expressionRegex.Matches(line);

                        // matches[0].Groups[1].ToString() = "exp1" or "ans3" or whatever
                        string expressionName = matches[0].Groups[1].ToString();
                        // matches[0].Groups[2].ToString() = type of variable (single value, range, list, …)
                        string expressionDefinition = matches[0].Groups[2].ToString();

                        string expressionResult = lambdaParser.Eval(expressionDefinition, varContext).ToString();

                        varContext[expressionName] = expressionResult;

                        replaceStrings.Add(expressionName, expressionResult);

                        if (answerRegex.IsMatch(line))
                        {
                            answerLabelsShuffleMap.Add(expressionName, expressionName);
                            answerTemporaryReplaceStrings.Add(expressionName, expressionResult);
                        }
                    }
                }

                // Now, if answers are not unique, increment countRestarts and try to generate once more
                var answerValues = answerTemporaryReplaceStrings.Values.ToList();
                noAnswersRepeat = (answerValues.Distinct().Count() == answerValues.Count());
                if (!noAnswersRepeat)
                {
                    countRestarts++;
                }
            }


            // If countRestarts is excedeed, assume that the problem has an error in ans definition
            if (countRestarts > maxRerolls)
            {
                throw new ApplicationException("Input variables keep generating duplicate answers.");
            }

            // If there were "ans0" etc. labels, then ans0 is the correct answer
            // Save its value before shuffling answers
            StringBuilder problemAnswer = new StringBuilder();
            if (varContext.ContainsKey("ans0"))
            {
                problemAnswer.Append(varContext["ans0"].ToString());
            }
            else if (varContext.ContainsKey("tof1"))
            {
                foreach (var variable in varContext)
                {
                    if (variable.Key.StartsWith("tof"))
                    {
                        problemAnswer.Append(variable.Value.ToString());
                    }
                }
            }


            // Implement answer shuffle
            if (answerLabelsShuffleMap.Count > 0)
            {
                Rng.DictShuffle(answerLabelsShuffleMap);

                foreach (var replaceString in replaceStrings)
                {
                    if (replaceString.Key.StartsWith("ans"))
                    {
                        replaceStrings[replaceString.Key] = answerTemporaryReplaceStrings[answerLabelsShuffleMap[replaceString.Key]];
                    }
                }
            }

            foreach (var replaceString in replaceStrings)
            {
                var toreplace = $"€{replaceString.Key}€";
                var replacewith = RemoveTrailingZeros(replaceString.Value);
                problemText.Replace(toreplace, replacewith);
            }

            // Append CORRECT ANSWER to problemText

            problemText.Insert(0, Environment.NewLine);
            problemText.Insert(0, @"\begin{samepage}");
            problemText.Insert(0, Environment.NewLine);

            problemText.AppendLine();
            problemText.AppendLine();
            problemText.AppendLine($"% CORRECT ANSWER: {problemAnswer}");
            problemText.AppendLine(@"\end{samepage}");
            problemText.AppendLine();

            return problemText.ToString();
        }

        private static object DynamicParseToNumber(string input)
        {
            int tryInt;
            decimal tryDecimal;

            if (int.TryParse(input, out tryInt))
            {
                return tryInt;
            }
            else if (decimal.TryParse(input, out tryDecimal))
            {
                return tryDecimal;
            }
            else
            {
                return null;
            }
        }

        private static string RemoveTrailingZeros(this string input)
        {
            if (input.Contains("."))
            {
                return input.TrimEnd('0').TrimEnd('.');
            }
            else
            {
                return input;
            }
        }
    }
}
