﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Interfaces;
using Shared.Models;

namespace Shared
{
    public class ExamMethods
    {
        private ExamModel _exam;
        private readonly IQuestionRepository _questions;
        private List<ProblemModel> _chosenProblems;

        public ExamMethods(IQuestionRepository questions)
        {
            _exam = new ExamModel();
            _questions = questions;
            _chosenProblems = new List<ProblemModel>();
        }

        #region Properties (and adjacent methods)

        public string Title
        {
            get { return _exam.Title; }
            set { _exam.Title = value; }
        }


        public IList<ProblemModel> ChosenProblems => _chosenProblems;

        public int Count => ChosenProblems.Count;

        public int OpenCount 
            => ChosenProblems.Where( problem => problem.Type == ProblemType.open ).Count();

        public int ClosedCount
            => ChosenProblems.Where(problem => problem.Type == ProblemType.closed).Count();

        public int TotalPoints() 
            => ChosenProblems.Select(problem => problem.Score).Sum();

        #endregion Properties (and adjacent methods)



        #region CRUD methods

        public void AddProblem(ProblemModel problemToAdd)
        {
            _chosenProblems.Add(problemToAdd);
            _exam.ChosenProblemIds.Add(problemToAdd.IdNumber);
        }

        public void AddProblem(ProblemModel problemToAdd, int indexToAdd)
        {
            if (indexToAdd < 0 || indexToAdd >= ChosenProblems.Count)
            {
                _chosenProblems.Add(problemToAdd);
                _exam.ChosenProblemIds.Add(problemToAdd.IdNumber);
            }
            else
            {
                _chosenProblems.Insert(indexToAdd, problemToAdd);
                _exam.ChosenProblemIds.Insert(indexToAdd, problemToAdd.IdNumber);
            }
        }

        public bool RemoveProblem(int indexToRemove)
        {
            bool status;
            if (indexToRemove < 0 || indexToRemove >= ChosenProblems.Count)
            {
                status = false;
            }
            else
            {
                _chosenProblems.RemoveAt(indexToRemove);
                _exam.ChosenProblemIds.RemoveAt(indexToRemove);
                status = true;
            }
            return status;
        }

        public void Clear()
        {
            _chosenProblems.Clear();
            _exam.ChosenProblemIds.Clear();
        }

        public bool Replace(ProblemModel problemToReplaceWith, int indexToReplace)
        {
            bool status;
            if (indexToReplace < 0 || indexToReplace >= ChosenProblems.Count)
            {
                status = false;
            }
            else
            {
                _chosenProblems[indexToReplace] = problemToReplaceWith;
                _exam.ChosenProblemIds[indexToReplace] = problemToReplaceWith.IdNumber;
                status = true;
            }
            return status;
        }

        #endregion CRUD methods



        #region Serializing

        public ExamModel SaveData()
        {
            return _exam;
        }

        public void LoadData(ExamModel exam)
        {
            _exam = exam;
            // Method ReadMultipleProblems returns a list, in which problems have
            // the same order as in _exam.ChosenProblemIds!
            _chosenProblems = _questions.ReadMultipleProblems(_exam.ChosenProblemIds).ToList();
            SanitizeLoadedData();
        }

        /*
        * It is possible that ExamModel object holds a reference to a problem that was later removed from
        * the repository. This is a not a big deal – in that case we just update the ExamModel object to include
        * only valid problems. This *might* have consequences if there is a database issue.
        */
        private void SanitizeLoadedData()
        {
            if (_exam.ChosenProblemIds.Count < _chosenProblems.Count)
            {
                _exam.ChosenProblemIds = _chosenProblems.Select(prob => prob.IdNumber).ToList();
            }
        }

        #endregion
    }
}
