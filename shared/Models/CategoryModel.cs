﻿using System.ComponentModel.DataAnnotations;

namespace Shared.Models
{
    /// <summary>
    /// Data model. Consists of <paramref name="CategoryId"/> and <paramref name="CategoryName"/>
    /// </summary>
    /// <param name="CategoryId">of int type</param>
    /// <param name="CategoryName">of string type</param>
    public class CategoryModel
    {
        [Key]
        [Display(Name = "Id działu")]
        public int CategoryId { get; set; }

        [MinLength(5, ErrorMessage = "Nazwa działu musi mieć co najmniej 5 znaków.")]
        [MaxLength(32, ErrorMessage = "Nazwa działu może mieć maksymalnie 32 znaki.")]
        [Display(Name = "Nazwa działu")]
        public string CategoryName { get; set; }
    }
}
