﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Shared.Models
{
    public enum ProblemType 
    {
        [Display(Name = "zamknięte")]
        closed,
        
        [Display(Name = "otwarte")]
        open 
    };
}


// If there are more enums, the following extension will have to be moved to its own class –
// the namespace is therefore already explicitly repeated
namespace Shared.Models
{
    public static class ExtensionMethods
    {
        public static string EnumPrettyLabel(this ProblemType prob)
        {
            return prob switch
            {
                ProblemType.closed => "zamknięte",
                ProblemType.open => "otwarte",
                _ => throw new ApplicationException($"Tried to access invalid enum value for {nameof(ProblemType)}."),
            };
        }
    }
}
