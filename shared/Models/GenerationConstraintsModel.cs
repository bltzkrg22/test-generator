﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models
{
    public class GenerationConstraintsModel
    {
        [Key]
        public Guid GUID { get; set; }

        [Required]
        [Range(0, int.MaxValue, ErrorMessage = "Liczba zadań musi być nieujemna.")]
        [Display(Name = "Liczba zadań zamkniętych")]
        public int ClosedCount { get; set; }

        [Required]
        [Range(0, int.MaxValue, ErrorMessage = "Liczba zadań musi być nieujemna.")]
        [Display(Name = "Liczba zadań otwartych")]
        public int OpenCount { get; set; }

        public HashSet<int> CategoryIdsSelected { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Tytuł testu nie może być pusty.")]
        [Display(Name = "Tytuł")]
        public string Title { get; set; }

    }
}
