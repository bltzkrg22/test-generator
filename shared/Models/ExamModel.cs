﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models
{
    public class ExamModel
    {
        [Key]
        public int DbId { get; set; }

        public List<int> ChosenProblemIds { get; set; }
        // public List<ProblemModel> ChosenProblems { get; set; }

        public string Title { get; set; }

        public ExamModel()
        {
            ChosenProblemIds = new List<int>();
        }
    }
}
