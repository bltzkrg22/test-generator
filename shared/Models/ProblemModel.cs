﻿using System.ComponentModel.DataAnnotations;

namespace Shared.Models
{
    public class ProblemModel
    {
        [Key]
        [Display(Name = "Id zadania")]
        public int IdNumber { get; set; }

        [Required]
        [Display(Name = "Liczba punktów")]
        [Range(1, int.MaxValue, ErrorMessage = "Liczba punktów musi być dodatnia.")]
        public int Score { get; set; }

        [Required(ErrorMessage = "Typ zadania musi zostać wskazany.")]
        [Display(Name = "Typ zadania (otwarte/zamknięte)")]
        public ProblemType Type { get; set; }

        [Required(ErrorMessage = "Dział musi zostać wskazany.")]
        [Display(Name = "Dział")]
        public CategoryModel Category { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Zadanie musi posiadać treść.")]
        [Display(Name = "Treść")]
        public string ProblemText { get; set; }

    }
}
