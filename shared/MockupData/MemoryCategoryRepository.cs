﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Shared.Models;
using Shared.Interfaces;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;

namespace Shared.MockupData
{
    public class MemoryCategoryRepository : ICategoryRepository, IRepository<CategoryModel>
    {
        private Dictionary<int, CategoryModel> categories;

        // _questions is required for DeleteCategory()

        public CategoryModel this[int id] => categories.GetValueOrDefault(id, null);

        public IEnumerable<CategoryModel> Categories => categories.Values;

        public IEnumerable<CategoryModel> RepositoryContents => categories.Values;

        public void CreateCategory(string newCategoryName)
        {
            int newId = categories.Count == 0 ? 1 : categories.Keys.Max() + 1;
            var category = new CategoryModel() { CategoryId = newId, CategoryName = newCategoryName };
            categories[newId] = category;
        }

        public CategoryModel ReadCategory(int id) => categories.GetValueOrDefault(id, null);

        public bool UpdateCategory(int id, string updatedCategoryName)
        {
            if (!categories.ContainsKey(id))
            {
                // This is Update method, if there is no problem with give id we return false
                return false;
            }
            else
            {
                var updatedCategory = new CategoryModel() { CategoryId = id, CategoryName = updatedCategoryName };
                categories[id] = updatedCategory;
                return true;
            }
        }

        public bool DeleteCategory(int id, IEnumerable<ProblemModel> problems = null)
        {
            // We can only remove a category if it's empty!
            // We need to check it first.

            bool operationStatus;
            if (!categories.ContainsKey(id))
            {
                operationStatus = false;
            }
            else if (problems.Select( prob => prob.Category).Where( selected => selected.CategoryId == id ).Any())
            {
                operationStatus = false;
            }
            else
            {
                categories.Remove(id);
                operationStatus = true;
            }

            return operationStatus;
        }


        public MemoryCategoryRepository()
        {
            categories = new Dictionary<int, CategoryModel>();

            SeedRepository();
        }

        private void SeedRepository()
        {
            categories = new Dictionary<int, CategoryModel>();

            CreateCategory("Geometria");
            CreateCategory("Algebra");
            CreateCategory("Arytmetyka");
        }


    }
}
