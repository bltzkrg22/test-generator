﻿using Shared.Interfaces;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.MockupData
{
    public class MemoryExamRepository : IExamRepository
    {
        private Dictionary<int, ExamModel> exams;


        public IEnumerable<ExamModel> GeneratedExams => exams.Values;


        #region CRUD operations

        public bool DeleteExam(int id)
        {
            bool operationStatus;
            if (exams.ContainsKey(id))
            {
                exams.Remove(id);
                operationStatus = true;
            }
            else
            {
                operationStatus = false;
            }
            return operationStatus;
        }

        public bool OverwriteExam(ExamModel examToOverwriteWith, int id)
        {
            bool operationStatus;
            if (exams.ContainsKey(id))
            {
                exams[id] = examToOverwriteWith;
                operationStatus = true;
            }
            else
            {
                operationStatus = false;
            }
            return operationStatus;
        }

        public ExamModel RetrieveExam(int id) => exams.GetValueOrDefault(id, null);

        public int StoreExam(ExamModel examToStore)
        {
            int newId = exams.Count == 0 ? 1 : exams.Keys.Max() + 1;
            examToStore.DbId = newId;
            exams[newId] = examToStore;
            return newId;
        }

        #endregion CRUD operations



        #region Constructors
        public MemoryExamRepository()
        {
            exams = new Dictionary<int, ExamModel>();
        }

        #endregion Constructors
    }
}
