﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Shared.Models;
using Shared.Interfaces;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;

namespace Shared.MockupData
{
    public class MemoryQuestionRepository : IQuestionRepository, IRepository<ProblemModel>
    {

        private Dictionary<int, ProblemModel> problems;
        private readonly ICategoryRepository _categories;


        #region Interface region

        public ProblemModel this[int id] => problems.GetValueOrDefault(id, null);

        public IEnumerable<ProblemModel> Problems => problems.Values;

        public IEnumerable<ProblemModel> RepositoryContents => problems.Values;

        public ProblemModel CreateProblem(ProblemModel problem)
        {
            // We want to add a new problem to the database. We *ignore* the supplied
            // Id, and instead overwrite with the next available Id
            int newId = problems.Count == 0 ? 1 : problems.Keys.Max() + 1;
            problem.IdNumber = newId;
            problems[newId] = problem;
            return problem;
        }

        // This is redundant due to indexer, but I just want to have a full CRUD
        public ProblemModel ReadProblem(int id) => problems.GetValueOrDefault(id, null);

        public IList<ProblemModel> ReadMultipleProblems(IList<int> ids)
        {
            return Problems.Join(ids, prob => prob.IdNumber, id => id, (prob, id) => prob).ToList();
        }

        public ProblemModel UpdateProblem(int id, ProblemModel problem)
        {
            if (!problems.ContainsKey(id))
            {
                // This is Update method, if there is no problem with give id we return null
                return null;
            }
            else
            {
                // Not sure if it’s smart, but I ignore id from problem and overwrite
                // with id. Maybe I should assert they are equal?
                problem.IdNumber = id;
                problems[id] = problem;
            }
            return problem;
        }

        public bool DeleteProblem(int id)
        {
            bool operationStatus;
            if (problems.ContainsKey(id))
            {
                problems.Remove(id);
                operationStatus = true;
            }
            else
            {
                operationStatus = false;
            }
            return operationStatus;
        }

        public IList<int> FilterIdsOfProblems(ProblemType type, ISet<int> selectedCategoryIds = null)
        {
            if (selectedCategoryIds is not null && selectedCategoryIds.Count > 0)
            {
                return Problems.Join(selectedCategoryIds, prob => prob.IdNumber, id => id, (prob, id) => prob)
                               .Where(prob => prob.Type == type)
                               .Select(prob => prob.IdNumber)
                               .ToList();
                // return Problems.Where(prob => prob.Type == type && selectedCategoryIds.Contains(prob.Category.CategoryId)).Select(prob => prob.IdNumber).ToList();
            }
            else
            {
                return Problems.Where(prob => prob.Type == type).Select(prob => prob.IdNumber).ToList();
            }
        }

        #endregion Inteface region


        #region Constructor region

        public MemoryQuestionRepository(IConfiguration configuration, ICategoryRepository catRepo)
        {
            problems = new Dictionary<int, ProblemModel>();
            _categories = catRepo;

            SeedRepository(configuration);
        }

        private void SeedRepository(IConfiguration configuration)
        {
            problems = new Dictionary<int, ProblemModel>();

            string rootDirectory = configuration.GetValue<string>(WebHostDefaults.ContentRootKey);

            var problem1 = new ProblemModel()
            {
                Score = 1,
                Type = ProblemType.closed,
                Category = _categories.ReadCategory(1),
                ProblemText = File.ReadAllText(Path.GetFullPath(Path.Combine(rootDirectory, @".\..\shared\MockupData\Problem001.tex")))
            };
            var problem2 = new ProblemModel()
            {
                Score = 1,
                Type = ProblemType.closed,
                Category = _categories.ReadCategory(1),
                ProblemText = File.ReadAllText(Path.GetFullPath(Path.Combine(rootDirectory, @".\..\shared\MockupData\Problem002.tex")))
            };
            var problem14 = new ProblemModel()
            {
                Score = 1,
                Type = ProblemType.open,
                Category = _categories.ReadCategory(2),
                ProblemText = File.ReadAllText(Path.GetFullPath(Path.Combine(rootDirectory, @".\..\shared\MockupData\Problem014.tex")))
            };

            CreateProblem(problem1);
            CreateProblem(problem2);
            CreateProblem(problem14); // This problem will have Id = 3, not 14!




            for (int i = 1; i <= 15; i++)
            {
                var rngScore = Rng.GetRandom(1, 6 + 1);
                var rngType = Rng.GetRandom() % 2 == 0 ? ProblemType.closed : ProblemType.open;
                var rngCat = Rng.GetRandom(1, 3 + 1);

                var problem3 = new ProblemModel()
                {
                    Score = rngScore,
                    Type = rngType,
                    Category = _categories.ReadCategory(rngCat),
                    ProblemText = File.ReadAllText(Path.GetFullPath(Path.Combine(rootDirectory, @".\..\shared\MockupData\Problem002.tex")))
                };

                CreateProblem(problem3);
            }
        }

        #endregion Constructor region
    }
}
