﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Helpers
{
    public static class TimeHelpers
    {
        public static long GetTimestampNumeric(DateTime? timeOrDefault = null)
        {
            DateTime time;
            if (timeOrDefault is null)
            {
                time = DateTime.Now;
            }
            else
            {
                time = timeOrDefault.Value;
            }
            DateTime unixStartDate = new DateTime(1970, 1, 1, 0, 0, 0);
            return Convert.ToInt64((DateTime.Now - unixStartDate).TotalSeconds);
        }

        public static string GetTimestamp(DateTime? timeOrDefault = null) 
            => GetTimestampNumeric(timeOrDefault).ToString();

        public static bool IsFileFresh(DateTime creationDate)
        {
            TimeSpan difference = DateTime.Now - creationDate;
            return difference.TotalMinutes <= LibraryConstants.FreshnessTimespan;
        }
    }
}
