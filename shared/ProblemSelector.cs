﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Interfaces;
using Shared.Models;

namespace Shared
{

    public static class ProblemSelector
    {
        /// <summary>
        /// A static method that will generate an Exam object given a database of questions an generation constraints.
        /// If a database does not have the number of questions specified in constraints, it returns all matches.
        /// </summary>
        public static ExamMethods SelectProblemsForExam(IQuestionRepository questions, GenerationConstraintsModel constraints)
        {
            var newExam = new ExamMethods(questions) { Title = constraints.Title };

            IList<int> potentialClosed = questions.FilterIdsOfProblems(ProblemType.closed, constraints.CategoryIdsSelected);
            Rng.KnuthShuffle(potentialClosed);

            List<int> closedIdsTaken = potentialClosed.Take(constraints.ClosedCount).ToList();
            IList<ProblemModel> closedProblemsTaken = questions.ReadMultipleProblems(closedIdsTaken);
            foreach (var problem in closedProblemsTaken)
            {
                newExam.AddProblem(problem);
            }


            IList<int> potentialOpen = questions.FilterIdsOfProblems(ProblemType.open, constraints.CategoryIdsSelected);
            Rng.KnuthShuffle(potentialOpen);

            List<int> openIdsTaken = potentialOpen.Take(constraints.OpenCount).ToList();
            IList<ProblemModel> openProblemsTaken = questions.ReadMultipleProblems(openIdsTaken);
            foreach (var problem in openProblemsTaken)
            {
                newExam.AddProblem(problem);
            }

            return newExam;
        }

        // A simple async wraparound for a normal synchronous method
        //public static async Task<Exam> GenerateExamAsync(IQuestionRepository questions, GenerationConstraintsModel constraints)
        //{
        //    return await Task.Run(() => SelectProblemsForExam(questions, constraints));
        //}

    }
}
