﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Shared.Helpers;
using Shared.Interfaces;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public class ExamPrinter
    {
        private ExamModel _exam;
        private MemoryStream _memFile;
        private StreamWriter _data;
        private readonly IConfiguration _configuration;
        private readonly IQuestionRepository _questions;

        public int NumberOfStudents { get; set; }


        #region Constructor region

        public ExamPrinter(IConfiguration configuration, IQuestionRepository questions)
        {
            _configuration = configuration;
            _exam = new ExamModel();
            _memFile = new MemoryStream();
            _data = new StreamWriter(_memFile);
            _questions = questions;
        }

        public void LoadExamModel(ExamModel exam)
        {
            this._exam = exam;
        }

        #endregion Constructor region


        private StringBuilder ExamHeaderFooterMutator(StringBuilder headerContent, Dictionary<string, string> replacementDict)
        {
            StringBuilder header = new StringBuilder(headerContent.Length + 128); // MAGIC NUMBER!

            // Remove % (LaTeX Comment) from beginning of the header definition
            string[] headerLineArray = headerContent.ToString().Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
            foreach (var line in headerLineArray)
            {
                if (line[0] == '%')
                {
                    header.AppendLine(line[1..]);
                }
                else
                {
                    header.AppendLine(line);
                }
            }

            foreach (var replacePair in replacementDict)
            {
                var toreplace = $"€{replacePair.Key}€";
                var replacewith = replacePair.Value;
                header.Replace(toreplace, replacewith);
            }

            return header;
        }

        private void PrintSingleStudent(StringBuilder header, StringBuilder footer, int studentNumber)
        {
            // MAGIC DICTIONARY
            // This can be extended to replace data in header and footer, but so far it only adds test number!
            var headerfooterDict = new Dictionary<string, string>() { { "var1", studentNumber.ToString() } };

            _data.WriteLine();

            _data.Write(ExamHeaderFooterMutator(header, headerfooterDict));
            _data.WriteLine();

            var chosenProblemsList = _exam.ChosenProblemIds.ToList();

            for (int i = 0; i < chosenProblemsList.Count; i++)
            {
                //_data.Write(ProblemMutator.ProblemTextMutator(chosenProblemsList[i].ProblemText));
                var currentProblem = _questions.ReadProblem(chosenProblemsList[i]);

                // It is possible that a problem was deleted from database after exam was generated
                if (currentProblem is not null)
                {
                    _data.Write(ProblemMutator.ProblemTextMutator(currentProblem.ProblemText));
                    _data.WriteLine();
                }
            }

            _data.Write(ExamHeaderFooterMutator(footer, headerfooterDict));
            _data.WriteLine();
        }

        public MemoryStream PrintLatexFile(StreamReader template)
        {
            //string rootDirectory = _configuration.GetValue<string>(WebHostDefaults.ContentRootKey);
            //string pathToTemplate = Path.Join(rootDirectory, @".\ExamFilesTemplates\Template.tex");
            //StreamReader inputFile = new StreamReader(pathToTemplate);

            // string timestamp = Timestamp.GetTimestamp();
            // string pathToOutput = Path.Join(rootDirectory, @$".\ExamFilesTemplates\Outputfile-{timestamp}.tex");

            StringBuilder header = new StringBuilder();
            StringBuilder footer = new StringBuilder();


            template.DiscardBufferedData();
            template.BaseStream.Seek(0, System.IO.SeekOrigin.Begin);

            string currentLine;
            while ((currentLine = template.ReadLine()) != null)
            {
                switch (currentLine)
                {
                    case "%%% HEADER":
                        header.AppendLine("%%% HEADER");
                        while ((currentLine = template.ReadLine()) != null && currentLine != "%%% END OF HEADER")
                        {
                            header.AppendLine(currentLine);
                        }
                        if (currentLine == null)
                        {
                            throw new ApplicationException(".tex template file ended while scanning for the end of header.");
                        }
                        else
                        {
                            header.AppendLine(currentLine);
                        }

                        _data.Write(header);
                        break;
                    case "%%% FOOTER":
                        footer.AppendLine("%%% FOOTER");
                        while ((currentLine = template.ReadLine()) != null && currentLine != "%%% END OF FOOTER")
                        {
                            footer.AppendLine(currentLine);
                        }
                        if (currentLine == null)
                        {
                            throw new ApplicationException(".tex template file ended while scanning for the end of footer.");
                        }
                        else
                        {
                            footer.AppendLine(currentLine);
                        }

                        _data.Write(footer);
                        break;
                    case "%%% REFERENCES":
                        _data.WriteLine(currentLine);

                        for (int i = 1; i <= NumberOfStudents; i++)
                        {
                            PrintSingleStudent(header, footer, i);
                        }
                        break;
                    default:
                        _data.WriteLine(currentLine);
                        break;
                }
            }

            template.Close();
            _data.Flush();
            _data.Close();

            return _memFile;
        }
    }
}
