﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public class LibraryConstants
    {
        public const int MaxMutatorRerolls = 5;

        // For how many minutes is a file considered fresh
        public const int FreshnessTimespan = 5;
    }
}
