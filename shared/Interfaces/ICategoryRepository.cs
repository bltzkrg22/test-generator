﻿using System.Collections.Generic;
using Shared.Models;

namespace Shared.Interfaces
{
    public interface ICategoryRepository
    {
        IEnumerable<CategoryModel> Categories { get; }

        CategoryModel this[int id] { get; }

        void CreateCategory(string newCategoryName);

        CategoryModel ReadCategory(int id);

        bool UpdateCategory(int id, string updatedCategoryName);

        // We need to have knowledge about problems database before deleting a category
        // Do not allow deleting a category that has problems linked!

        // IEnumerable<ProblemModel> problems parameter is required only for inmemory repository!
        bool DeleteCategory(int id, IEnumerable<ProblemModel> problems);
    }
}