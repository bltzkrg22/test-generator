﻿using System.Collections.Generic;
using Shared.Models;

namespace Shared.Interfaces
{
    public interface IQuestionRepository
    {
        IEnumerable<ProblemModel> Problems { get; }
        ProblemModel this[int id] { get; }
        ProblemModel CreateProblem(ProblemModel problem);
        ProblemModel ReadProblem(int id);
        IList<ProblemModel> ReadMultipleProblems(IList<int> ids);
        ProblemModel UpdateProblem(int id, ProblemModel problem);
        bool DeleteProblem(int id);
        IList<int> FilterIdsOfProblems(ProblemType type, ISet<int> selectedCategoryIds);
    }
}