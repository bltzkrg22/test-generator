﻿using System.Collections.Generic;
using Shared.Models;

namespace Shared.Interfaces
{
    public interface IExamRepository
    {
        IEnumerable<ExamModel> GeneratedExams { get; }

        int StoreExam(ExamModel examToStore);

        ExamModel RetrieveExam(int id);

        bool OverwriteExam(ExamModel examToOverwriteWith, int id);

        bool DeleteExam(int id);
    }
}