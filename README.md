# Test Generator

A web application that demonstrates the usage of ASP.NET Core MVC framework.

It enables the user to create customized tests/exams, where the problems can be randomized (or explicitly chosen) from a connected database. The app outputs the intermediate file `.tex` files, that later must be compiled by the user to PDF format.

Since this is only a demo project, and the compilation process is pretty CPU-consuming anduninteresting (as simple as running command: `latexmk -xelatex -synctex=0  -cd -file-line-error <inputfile>`), the app stops at `.tex` file stage. But if required, a server or just a VPS with a LaTeX distribution could be used.

-----

The application uses Entity Framework as the ORM for data access. CRUD operations on the database are all availabe through the application UI, though there is no support for batch operations.

(Note that while all the code required to use the SQL database is already implemented in `sql-access-library` and ready to use after switch a few lines in the startup file – the demo version deployed to Azure uses a transient in-memory storage, so that the data may be safely and quickly modified, erased and later restored. The demo version is available on the `demo-nologin` git branch).

## Usage

The detailed instructions on how to use the app are [available in-app](https://bltzkrg22-test-generator.azurewebsites.net/Home/Readme).

## Additional information

The application is deployed to Azure Web Services.
